import deliah_about from '../common/deliah_about.svg';
import bouquet_about from '../common/bouquet_about.svg';
import poppies_about from '../common/poppies_about.svg';
import '../about.css';


function About() {
    return (
      <main>
        <div className='container-fluid'>
          <div className='grid-container-about'>
            <div className='row-one-column-one'>
              <img src={deliah_about} alt='deliah-image' className='deliah-image' />
            </div>
            <div className='about-text' >
                <h2 style={{fontFamily: 'RoadRunner', fontWeight: 'bold'}}>About the Artist</h2>
                <h5 style={{fontFamily: 'RoadRunner'}}>Deliah Hubbard</h5>
                <p> Nunc vel ligula rhoncus, condimentum enim eget, vestibulum lectus. Phasellus at quam eu dui vestibulum condimentum ut at diam. In hac habitasse platea dictumst. Vestibulum faucibus purus ut elementum vehicula. Nulla sit amet bibendum orci. Sed eget ipsum nec metus ornare porta. Quisque elementum placerat arcu, sit amet semper augue c</p>
                <p> Nunc vel ligula rhoncus, condimentum enim eget, vestibulum lectus. Phasellus at quam eu dui vestibulum condimentum ut at diam. In hac habitasse platea dictumst. Vestibulum faucibus purus ut elementum vehicula. Nulla sit amet bibendum orci. Sed eget ipsum nec metus ornare porta. Quisque elementum placerat arcu, sit amet semper augue c</p>
                <p> Nunc vel ligula rhoncus, condimentum enim eget, vestibulum lectus. Phasellus at quam eu dui vestibulum condimentum ut at diam. In hac habitasse platea dictumst. Vestibulum faucibus purus ut elementum vehicula. Nulla sit amet bibendum orci. Sed eget ipsum nec metus ornare porta. Quisque elementum placerat arcu, sit amet semper augue c</p>
            </div>
          <div className='image-row'>
              <img src={bouquet_about} alt='bouquet-about' className='bouquet-about-pic' />
          </div>
          <div>
            <img src={bouquet_about} alt='bouquet-about' className='bouquet-about-pic' />

          </div>


          </div>
        </div>
      </main>
    );
  }

  export default About;
