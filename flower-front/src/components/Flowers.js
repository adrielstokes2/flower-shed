import homeFlowers from '../common/homeFlowers.png';
import '../flowers.css';

function Flowers() {
    return (
      <main>
        <div className="container-fluid">
          <div className="grid-container">
            <div className="first-column-first-square">
              <h1>does a bunch of info change the layout at all pleas say no</h1>
            </div>
            <div className="first-column-second-square">
              <h1>Square 2</h1>
            </div>
            <div className="second-column-flex">
              <div className="rectangle">
              </div>
              <div className="rectangle">
                <h1>Rectangle 2</h1>
              </div>
              <div className="rectangle">
                <h1>Rectangle 3</h1>
              </div>
            </div>
            <div className="third-column-first-square">
              <h1>Square 3</h1>
            </div>
            <div className="third-column-second-square">
              <h1>Square 4</h1>
            </div>
          </div>
        </div>
      </main>
    );
  }

  export default Flowers;
