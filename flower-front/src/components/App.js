import 'bootstrap/dist/css/bootstrap.min.css';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import NavBar from './NavBar';
import '../index.css';
// import Masonry from 'masonry-layout';
import Flowers from './Flowers';
import Home from './Home';
import About from './About';
import Classes from './Classes';
import Watercolors from './Watercolors';
import Footer from './Footer';





function App() {
  return (
    <BrowserRouter>
      <NavBar />
      <div>
        <Routes>
          <Route path="home/" element={<Home />} />
          <Route path="about/" element={<About />} />
          <Route path="classes/" element={<Classes />} />
          <Route path="watercolors/" element={<Watercolors />} />
          <Route path="flowers/" element={<Flowers />} />
        </Routes>
      </div>
      <Footer />
    </BrowserRouter>

  );
}

export default App;
