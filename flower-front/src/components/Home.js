
import homeTeaching from '../common/homeTeaching.svg';
import flowerOrders from '../common/flowerOrders.png';
import frame47 from '../common/frame47.svg';
import home_bouquet from '../common/home_bouquet.svg';
import home_flowers_painting from '../common/home_flowers_painting.svg';
import arrow_right_solid from '../common/arrow_right_solid.svg';
import '../home.css';



function Home() {
    return (
      <main>
        <div className="container-fluid">
          <div className="grid-container-home">
            <div className="first-column-first-square-home">
                <img src={frame47} alt="shed-pic-home" className="shed-square-image" />
            </div>
            <div className="third-column-rectangles-home">
              <div className="rectangle-home">
                <img src={flowerOrders} alt="flower-button-home" className="flowers-orders-square-image" />
              </div>
              <div className="rectangle-home">
                <h1>Home Rectangle 2</h1>
              </div>
              <div className="rectangle-home">
                <h1>Home Rectangle 3</h1>
              </div>
            </div>

            <div className="first-column-second-square-home">
              <div className='flowers-text-overlay'>
                <h2>Flowers</h2>
                <img src={arrow_right_solid} alt="flowers-button" className='arrows'></img>
              </div>
               <img src={home_bouquet} alt="flower-pic-home" className="home-bouquet-image" />
                <p style={{marginLeft: '14px', marginRight: '14px'}}> bouquets long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Man</p>
            </div>

            <div className="second-column-square-home">
              <div className='watercolors-text-overlay'>
                <h2>Watercolors</h2>
                <img src={arrow_right_solid} className='arrows'   ></img>
              </div>
                <img src={home_flowers_painting} alt="jackson-pic-home" className="jackson-square-image" />
                <p style={{marginLeft: '14px', marginRight: '14px'}}> flowers a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Man</p>
            </div>



            <div className="third-column-second-square-home">
              <div className='classes-text-overlay'>
                <h2>Classes & Events</h2>
                <img src={arrow_right_solid} alt="classes-button" className='arrows'   ></img>
              </div>
                <img src={homeTeaching} alt="teaching-pic-home" className="teaching-square-image" />
                <p style={{marginLeft: '14px', marginRight: '14px'}}>classes a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Man</p>
            </div>

          </div>

        </div>
      </main>
    );
  }

  export default Home;
