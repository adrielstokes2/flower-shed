import { NavLink, Link } from 'react-router-dom';
import shapeCircle from '../common/shape_circle.png';
import house_solid from '../common/house_solid.svg';
import user_solid from '../common/user_solid.svg';
import palette_solid from '../common/palette_solid.svg';
import school_solid from '../common/school_solid.svg';
import flower_solid from '../common/flower_solid.svg';
import logo from '../common/logo.svg';
import contact from '../common/contact.svg';

function NavBar() {
   return (
        <nav className="navbar navbar-expand-lg" style ={{backgroundColor: '#FDF1F1', marginBottom: '10px'}}>
            <div className="container-fluid">
                <div className ="ms-3">
                    <img src={logo} alt="Shape Circle" style={{width: '80px', height: '80px'}}  />
                </div>
                <div className="collapse navbar-collapse" id="navbarNav">
                    <ul className="navbar-nav ms-auto mx-5" style={{fontFamily: 'Onest', fontWeight: 'bold'}}>
                        <li className="nav-item mx-3">
                            <NavLink to="/home" className="nav-link" activeClassName="active" exact>
                                <img src={house_solid} alt="Home" style={{ width: '22px', height: '22px', marginRight: '5px' }} /> Home
                            </NavLink>
                        </li>
                        <li className="nav-item mx-3">
                            <NavLink to='/about'className="nav-link" href="#">
                                <img src={user_solid} alt="User" style={{ width: '22px', height: '22px', marginRight: '5px' }} /> About
                            </NavLink>
                        </li>
                        <li className="nav-item mx-3">
                            <NavLink to='/flowers' className="nav-link" href="#">
                                <img src={flower_solid} alt="User" style={{ width: '22px', height: '22px', marginRight: '5px' }} /> Flowers

                            </NavLink>
                        </li>

                        <li className="nav-item mx-3">
                            <NavLink to='/watercolors'className="nav-link" href="#">
                                <img src={palette_solid} alt="User" style={{ width: '22px', height: '22px', marginRight: '5px' }} /> Watercolors
                            </NavLink>
                        </li>

                        <li className="nav-item mx-3">
                            <NavLink to='/classes'className="nav-link" href="#">
                                <img src={school_solid} alt="User" style={{ width: '22px', height: '22px', marginRight: '5px' }} /> Classes
                            </NavLink>
                        </li>
                        <li className="nav-item mx-3">
                            <img src={contact} alt="User" style={{ width: '40px', height: '40px', margin: '2px', borderRadius: '50%', display: 'inline-block',}} />

                        </li>
                    </ul>
                </div>
            </div>
        </nav>

    );
}

export default NavBar;
