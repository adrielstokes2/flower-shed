import { NavLink } from 'react-router-dom';
import shapeCircle from '../common/shape_circle.png';


function Footer() {
    return (
        <footer className="footer" style={{fontFamily: 'Onest', fontWeight: 'bold'}}>
            <div className="footer-brand">
                <img src={shapeCircle} alt="Shape Circle" className="footer-logo" />
                <h2 className="footer-title">The Flower Shed</h2>
            </div>
            <div className='footer-first-column'>
                <h3>Contact</h3>
                <ul>
                    <li>406-250-7135</li>
                    <li>deliahalbee@gmail.com</li>
                </ul>
                <h3 style={{marginTop: '18px'}}>Social Media</h3>
                <ul>
                    <li>406-250-7135</li>
                    <li style={{marginBottom: '50px'}}>deliahalbee@gmail.com</li>
                </ul>
            </div>

            <div className='footer-second-column'>
                <h3>Site</h3>
                <ul className='footer-links' style={{display: 'block'}}>
                    <li className='footer-link-home' id="footerHome">
                        <NavLink to="/home" className="nav-link-footer" activeClassName="active" exact>Home</NavLink>
                    </li>
                    <li className='footer-link-about'>
                        <NavLink to="/about" className="nav-link-footer" activeClassName="active" exact>About</NavLink>
                    </li>
                    <li className='footer-link-flowers'>
                        <NavLink to="/flowers" className="nav-link-footer" activeClassName="active" exact>Flowers</NavLink>
                    </li>
                    <li className='footer-link-watercolors'>
                        <NavLink to="/watercolors" className="nav-link-footer" activeClassName="active" exact>Watercolors</NavLink>
                    </li>
                    <li className='footer-link-classes'>
                        <NavLink to="/classes" className="nav-link-footer" activeClassName="active" exact>Classes</NavLink>
                    </li>
                </ul>
            </div>
            
            <div className="footer-newsletter">
                <h3>Don't Miss a Class!</h3>
                <p>Sign up for our mailing list and never miss a classes and upcoming event.</p>
                <form className="newsletter-form">
                    <input type="email" placeholder="Email Address" className="newsletter-input" />
                    <button type="submit" className="newsletter-button">SIGN UP NOW</button>
                </form>
            </div>
        </footer>
    );
}

export default Footer;
