from django.db import models

# Create your models here:
class ArtClasses(models.Model):
    booked = models.BooleanField(default=False, null=False)
    date = models.DateTimeField(max_length=50, null=True, blank=True)
    class_conducted = models.BooleanField(default=False, null=True)
    class_canceled = models.BooleanField(default=False, null=True)
    size = models.CharField(max_length=100, null=True)
    gross_expected_income = models.CharField(max_length=100, null=True)
    public_notes = models.CharField(max_length=900, null=True)
    private_notes = models.CharField(max_length=900, null=True)


class FormInquiries(models.Model):
    name = models.CharField(max_length=100, null=True)
    email = models.CharField(max_length=100, null=True)
    phone = models.CharField(max_length=100, null=True)
    class_dates = models.CharField(max_length=100, null=True)
    questions_or_inquiries = models.CharField(max_length=100, null=True)
    todays_date = models.CharField(max_length=100, null=True)




# End models here.
