from django.urls import path, include
from .views import api_list_all_art_classes, api_create_art_class, api_delete_art_class, api_list_specific_art_class, api_edit_art_class
from .views import api_list_all_form_inquiries, api_create_inquiry, api_list_specific_inquiry, api_delete_inquiry, api_list_art_classes_by_month

urlpatterns = [
    #art class urls:
    path('classes/', api_list_all_art_classes, name = 'api_list_all_classes'), # type: ignore 3
    path('create_class/', api_create_art_class, name = 'api_create_class'), # type: ignore
    path('classes/<int:id>/', api_delete_art_class, name = 'api_delete_classes'), # type: ignore
    path('class/<int:id>/', api_list_specific_art_class, name='api_list_specific_class'), # type: ignore
    path('classes/edit/<int:id>/', api_edit_art_class, name='api_edit_art_class' ), # type: ignore
    path('classes/<int:year>/<int:month>/', api_list_art_classes_by_month, name='api_list_art_classes_by_month'), # type: ignore
    #inquiry urls:
    path('inquiries/', api_list_all_form_inquiries, name = 'api_list_all_form_inquiries'), # type: ignore
    path('create_inquiry/', api_create_inquiry, name='api_create_inquiry'), # type: ignore
    path('inquiry/<int:id>/', api_list_specific_inquiry, name='api_list_specific_inquiry'), # type: ignore
    path('delete_inquiry/<int:id>/', api_delete_inquiry, name = "api_delete_inquiry"), # type: ignore
]
