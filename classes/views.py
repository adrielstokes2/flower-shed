from django.shortcuts import render
from django.views.decorators.http import require_http_methods
import json
from .json import ModelEncoder
from django.http import JsonResponse
from .models import ArtClasses, FormInquiries


# Model encoders start here:

class ArtClassesEncoder(ModelEncoder):
    model = ArtClasses
    properties = [
        "booked",
        "date",
        "class_conducted",
        "class_canceled",
        "size",
        "gross_expected_income",
        "private_notes",
        "public_notes",
        "id"
    ]

class FormInquiriesEncoder(ModelEncoder):
    model = FormInquiries
    properties = [
        "name",
        "email",
        "phone",
        "class_dates",
        "questions_or_inquiries",
        "todays_date",
        "id"
    ]

# Model encoders end here.


# All views start here:
######### Views for ArtClass objects start here: #######################

@require_http_methods(["GET"])
def api_list_all_art_classes(request):
    if request.method == "GET":
        art_classes = ArtClasses.objects.all()
        return JsonResponse(
            {"art_classes": art_classes},
            encoder= ArtClassesEncoder
        )


@require_http_methods(["GET"])
def api_list_specific_art_class(request, id):
    if request.method == "GET":
        art_classes = ArtClasses.objects.get(id=id)
        return JsonResponse(
            {"art_classes": art_classes},
            encoder=ArtClassesEncoder
        )


@require_http_methods(["POST"])
def api_create_art_class(request):
    if request.method == "POST":
        art_classes_content = json.loads(request.body)
        art_classes = ArtClasses.objects.create(**art_classes_content)
        return JsonResponse(
            art_classes,
            encoder=ArtClassesEncoder,
            safe = False
        )


@require_http_methods(["DELETE"])
def api_delete_art_class(request, id):
    if request.method == "DELETE":
        art_classes = ArtClasses.objects.get(id=id)
        art_classes.delete()
        return JsonResponse(
            art_classes,
            encoder=ArtClassesEncoder,
            safe = False
        )


@require_http_methods(["PUT"])
def api_edit_art_class(request, id):
    if request.method == "PUT":
        art_class = ArtClasses.objects.get(id=id)
        data = json.loads(request.body)
        for field, value in data.items():
            setattr(art_class, field, value)
        art_class.save()
        return JsonResponse(
            art_class,
            encoder=ArtClassesEncoder,
            safe=False
        )

@require_http_methods(["GET"])
def api_list_art_classes_by_month(request, year, month):
    if request.method == "GET":
        art_classes = ArtClasses.objects.filter(date__year=year, date__month=month)
        return JsonResponse(
            {"art_classes": art_classes},
            encoder=ArtClassesEncoder
        )


######### Views for ArtClass objects end here #######################
######### Views for FormInquiries objects start here #######################


@require_http_methods(["GET"])
def api_list_all_form_inquiries(request):
    if request.method == "GET":
        inquiries = FormInquiries.objects.all()
        return JsonResponse(
            {"inquiries": inquiries},
            encoder=FormInquiriesEncoder
        )


@require_http_methods(["GET"])
def api_list_specific_inquiry(request, id):
    if request.method == "GET":
        inquiry = FormInquiries.objects.get(id=id)
        return JsonResponse(
            {"inquiry": inquiry},
            encoder=FormInquiriesEncoder
        )


@require_http_methods(["POST"])
def api_create_inquiry(request):
    if request.method == "POST":
        inquiry_content = json.loads(request.body)
        inquiry = FormInquiries.objects.create(**inquiry_content)
        return JsonResponse(
            inquiry,
            encoder=FormInquiriesEncoder,
            safe = False
        )


@require_http_methods(["DELETE"])
def api_delete_inquiry(request, id):
    if request.method == "DELETE":
        inquiry = FormInquiries.objects.get(id=id)
        inquiry.delete()
        return JsonResponse(
            inquiry,
            encoder = FormInquiriesEncoder,
            safe = False
        )


######### Views for FormInquiries objects end here #######################



# Views end here.
