from django.contrib import admin
from .models import ArtClasses, FormInquiries

# Register your models here.
admin.site.register(ArtClasses)
admin.site.register(FormInquiries)
